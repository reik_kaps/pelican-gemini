import logging
from pelican import signals
import re
import os
from pathlib import Path


log = logging.getLogger(__name__)

try:
    from md2gemini import md2gemini
    enabled = True
except ImportError as e:
    log.error('can not import module: %s', e)
    enabled = False


def gemini_folder(path):
    """
    make sure gemini output folder exists
    param: path
    """
    if not os.path.exists(path):
        os.mkdir(path)


def build_gemtext(article_generator, content):
    """
    build gemtext from markdown source files
    which is referenced in the content object
    param: article_generator
    param: content
    """
    global enabled
    if not enabled:
        return

    out_path = article_generator.settings.get('GEMTEXT_PATH', 'gemini/')
    # Load a markdown file's contents into memory and get conversion
    log.debug("Markdown Relative Path: {}".format
              (content.relative_source_path))
    log.debug("Gemini Out path: {}".format
              (os.path.join(out_path, content.relative_source_path)))

    gemini_folder(out_path)

    fullpath = os.path.join(out_path, content.relative_source_path)
    log.debug("fullpath: {}".format
              (fullpath))

    md_text = ''
    log.debug("content.source_path: {}".format(content.source_path))
    with open(content.source_path, "r") as f:
        # write metadata
        md_text = '# {}'.format(content.metadata['title'])
        try:
            md_text = '{}\nvon {} | {}\n\n'.format(md_text,
                                                   content.metadata['author'],
                                                   content.metadata['date'])
        except KeyError:
            pass
        
        # remove the header with all metadata        
        for line in f:
            if not re.search('^[a-zA-Z\_]*\:', line):                
                md_text = md_text + line

    log.debug("content: %s", content.metadata)
    
    gemini = md2gemini(md_text,
                       plain=True,
                       links='paragraph',
                       strip_html=True)
    gmi_rel = Path(fullpath)
    
    log.debug("gemini: {}".format(gemini))
    with open(gmi_rel, 'w') as f:
        f.write(gemini)

    # rename the file to .gmi
    gmi_rel.rename(gmi_rel.with_suffix('.gmi'))

    
def register():
    signals.article_generator_write_article.connect(build_gemtext)

